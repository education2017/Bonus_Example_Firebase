package hr.ferit.bruno.example_firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemLongClick;

public class MainActivity extends AppCompatActivity implements ValueEventListener {

    private static String MESSAGES = "messages";
    private DatabaseReference mDatabaseReference;

    @BindView(R.id.ibSendMessage) ImageButton ibSendMessage;
    @BindView(R.id.etMessageAuthor) EditText etMessageAuthor;
    @BindView(R.id.etMessageBody) EditText etMessageBody;
    @BindView(R.id.lvMessages) ListView lvMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.mDatabaseReference = FirebaseDatabase.getInstance().getReference(MESSAGES);
        this.mDatabaseReference.addValueEventListener(this);
    }

    @OnClick(R.id.ibSendMessage)
    public void sendMessage(){
        String id = this.mDatabaseReference.push().getKey();
        String body = this.etMessageBody.getText().toString();
        String author = this.etMessageAuthor.getText().toString();
        Message message = new Message(id, body, author);
        this.mDatabaseReference.child(id).setValue(message);
        this.etMessageAuthor.setText("");
        this.etMessageBody.setText("");
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        List<Message> messageList = new ArrayList<>();
        for(DataSnapshot child : dataSnapshot.getChildren()){
            Message message = child.getValue(Message.class);
            messageList.add(message);
        }
        ArrayAdapter<Message> adapter = new ArrayAdapter<Message>(
                this,android.R.layout.simple_list_item_1,messageList);
        this.lvMessages.setAdapter(adapter);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    @OnItemLongClick(R.id.lvMessages)
    public boolean deleteMessage(int position){
        Message message = (Message) lvMessages.getAdapter().getItem(position);
        this.mDatabaseReference.child(message.getId()).removeValue();
        return true;
    }
}
